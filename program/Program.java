package program;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * These change depending on your package and filenames.
 */
import lexical.Lexical;
import lexical.Token;
import lexical.LexicalConstants;

/**
 * For the symbol table.
 */
import java.util.Map;
import java.util.HashMap;

public class Program {
    public static Map<String, Integer> table = new HashMap<String, Integer>();
    public static Integer tableIndex = 0;

    public static void main(String[] args) {
	try {
	    File file = new File("program.txt");
	    Lexical lexical = new Lexical(new FileInputStream(file));
	    Token token = Lexical.getNextToken();
	    String kind = LexicalConstants.tokenImage[token.kind];
	    String image = token.image;
	    while (token.kind != LexicalConstants.EOF) {
		if (token.kind == LexicalConstants.VARIABLE) { // <--- REPLACE FOR YOUR VARIABLE NAME
		    if (!table.containsKey(image)) {
			table.put(image, tableIndex++);
		    }
		    Integer index = table.get(image);
		    System.out.println(String.format("%s %d", kind, index));
		} else {
		    System.out.println(String.format("%s %s", kind, image));
		}
		token = Lexical.getNextToken();
		kind = LexicalConstants.tokenImage[token.kind];
		image = token.image;
	    }
	} catch (Exception exception) {
	    System.out.println("Can't read file");
	}
    }
}
