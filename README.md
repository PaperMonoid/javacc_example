# Instructions
You must download and install javacc to run this project.

# Lexical analyzer
First compile `lexical/Lexical.jj` with javacc. Then compile the java files with javac. After that, switch to this directory and compile `program/Program.java` with javac. Then you can run the program with java.

Example:
```shell
# change directory to lexical
cd lexical

# compile lexical with javacc
javacc Lexical.jj

# compile lexical java files with javac
javac *.java

# change directory to previous one
cd ..

# compile the main program
javac program/Program.java

# run the program
java program/Program
```
